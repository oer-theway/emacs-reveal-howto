// SPDX-FileCopyrightText: 2018-2019 Albert
// SPDX-License-Identifier: CC-BY-SA-4.0

quiz = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "What do you know about the template?  Let's see...",
        "level1":  "Excellent!", // 80-100%
        "level2":  "Well done!", // 60-79%
        "level3":  "You may need to re-read some parts.", // 40-59%
        "level4":  "Did you just jump here?",             // 20-39%
        "level5":  "Please restart from the beginning."   // 0-19%, no comma here
    },
    "questions": [
        { // Question 1 - Multiple Choice, Single True Answer
            "q": "Where are the 10 commandements found in scripture?",
            "a": [
                {"option": "In Genesis", "correct": false},
                {"option": "In Revelations", "correct": false},
                {"option": "In Deuteronomy", "correct": true},
                {"option": "In Exodus", "correct": true} // no comma here
            ],
            "correct": "<p><span>Correct!</span> Yes, they appear both in Deuteronomy and Exodus!</p>",
            "incorrect": "<p><span>No.</span> You may want to start over.</p>" // no comma here
        },
        { // Question 2 - Multiple Choice, Multiple True Answers, Select All
            "q": "Where the Law was given? (Select ALL.)",
            "a": [
                {"option": "At the mount Sinai", "correct": true},
                {"option": "At the mount of Olives", "correct": false},
                {"option": "At the well", "correct": false},
                {"option": "At the gates of the city of Jerusalem", "correct": false},
                {"option": "At the city center", "correct": false},
                {"option": "At the lake", "correct": false} // no comma here
            ],
            "correct": "<p><span>Correct!</span> You got it.</p>",
            "incorrect": "<p><span>Not Quite.</span> Try again.</p>" // no comma here
        } // no comma here
    ]
};
